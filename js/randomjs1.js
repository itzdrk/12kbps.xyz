var link = [];
link[0] = "../css/style1.css";
link[1] = "../css/style2.css";
link[2] = "../css/style3.css";
link[3] = "../css/style4.css";
link[4] = "../css/style5.css";


$(function() {
    var style = link[Math.floor(Math.random() * link.length )];
    if (document.createStyleSheet){
        document.createStyleSheet(style);
    }else{
        $('<link />',{
            rel :'stylesheet',
            type:'text/css',
            href: style
        }).appendTo('head');
    }
});
